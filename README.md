NY School app project. 

This is an android application using java. 

Main goal of the application: 
1. Show a list of schools in New York 
2. Display ditails for each of the schools. Including description, SAT scores. 



Items to do: 
1. Export all the XML strings, sizes, colors to resources. 
2. Convert the data storage into a local Room database 
   > Pass only school id to SchoolDetails class instead of the full object. 
3. Review Actions (Call, Email, Website, Maps) in the details page.
   > Check for permitions
   > Check for null values
4. Add search functionality


<a href="https://ibb.co/6w5hHzn"><img src="https://i.ibb.co/V98ygRj/Screenshot-2.png" alt="Screenshot-2" border="0"></a>
<a href="https://ibb.co/kg5zRYN"><img src="https://i.ibb.co/xSXwpVx/Screenshot-1.png" alt="Screenshot-1" border="0"></a>
