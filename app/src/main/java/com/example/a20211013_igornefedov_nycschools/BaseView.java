package com.example.a20211013_igornefedov_nycschools;

public interface BaseView<T> {
    void setPresenter(T Presenter);
    void initViews();//initializes the view components in the activity/fragment
}
