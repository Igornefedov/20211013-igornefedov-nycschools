package com.example.a20211013_igornefedov_nycschools.Screens.SchoolDetails;

import com.example.a20211013_igornefedov_nycschools.BasePresenter;
import com.example.a20211013_igornefedov_nycschools.BaseView;
import com.example.a20211013_igornefedov_nycschools.Data.SchoolModel;
import java.util.ArrayList;

public class SchoolDetailsContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter {
        void setSelectedSchool(SchoolModel schoolModel);
        SchoolModel getSelectedSchool();
    }

}
