package com.example.a20211013_igornefedov_nycschools.Screens.MainScreen;

import androidx.appcompat.app.AppCompatActivity;
import com.example.a20211013_igornefedov_nycschools.Data.DataSource;
import com.example.a20211013_igornefedov_nycschools.Data.SchoolModel;
import java.util.ArrayList;
import java.util.List;

public class MainPresenter implements MainContract.Presenter{

    private MainContract.View view;
    private DataSource dataSource;
    private String searchQuery;//TODO: Add searching capabilities

    MainPresenter(MainContract.View view){
        this.view = view;
        dataSource = new DataSource((AppCompatActivity) view);
    }

    @Override
    public void loadAllSchools() {

    }

    @Override
    public ArrayList<SchoolModel> getSchools() {
        return dataSource.getAllSchools();

        //test data:
//        ArrayList<SchoolModel> testData = new ArrayList<>();
//        testData.add(new SchoolModel("sjdf", "School #75", "this is an awesome schol"));
//        testData.add(new SchoolModel("fds", "School #77", "this is an 78 awesome schol"));
//        testData.add(new SchoolModel("sf2", "School #73", "this is an 70 awesome schol"));
//        return testData;
    }

    @Override
    public void start() {
        //TODO: load the schools here.
        view.initViews();
        dataSource.getData(success -> view.notifyDataChanged());
    }
}
