package com.example.a20211013_igornefedov_nycschools.Screens.SchoolDetails;

import com.example.a20211013_igornefedov_nycschools.Data.SchoolModel;

public class SchoolDetailsPresenter implements SchoolDetailsContract.Presenter{

    SchoolDetailsContract.View view;
    SchoolModel selectedSchool;

    public SchoolDetailsPresenter(SchoolDetailsContract.View view){
        this.view = view;
    }

    //

    @Override
    public void start() {
        //get all the needed data
        view.initViews();
    }

    @Override
    public void setSelectedSchool(SchoolModel schoolModel) {

        //todo: Replace this with a db call
        this.selectedSchool = schoolModel;
    }

    @Override
    public SchoolModel getSelectedSchool() {
        return selectedSchool;
    }
}
