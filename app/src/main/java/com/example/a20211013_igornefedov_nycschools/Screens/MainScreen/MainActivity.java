package com.example.a20211013_igornefedov_nycschools.Screens.MainScreen;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.a20211013_igornefedov_nycschools.R;

public class MainActivity extends AppCompatActivity implements MainContract.View{

    private MainPresenter presenter;
    private RecyclerView recyclerView;
    SchoolListAdapter listAdapter;

    //TODO:
    //1. Review the XML Layout and export all the Strings, sizes and colors into resources

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setPresenter(this);
    }

    @Override
    public void showLoading(boolean show) {
        //TODO: Add a spinner when it's loading
    }

    @Override
    public void notifyDataChanged() {
        //TODO: Update the list to match search key value
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void setPresenter(Object presenter) {
        this.presenter = new MainPresenter(this);
        this.presenter.start();
    }

    @Override
    public void initViews() {
        //todo: initialize all the views here.
        recyclerView = findViewById(R.id.recyclerView);
        listAdapter = new SchoolListAdapter(MainActivity.this, presenter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(listAdapter);
    }

}