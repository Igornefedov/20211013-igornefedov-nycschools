package com.example.a20211013_igornefedov_nycschools.Screens.MainScreen;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.example.a20211013_igornefedov_nycschools.R;
import com.example.a20211013_igornefedov_nycschools.Screens.SchoolDetails.SchoolDetails;

public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private MainPresenter presenter;
    private Context context;

    SchoolListAdapter(Context context, MainPresenter presenter){
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.presenter = presenter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        View view;
        TextView schoolName;
        TextView schoolDescription;
        ImageView schoolImage;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            view = itemView;
            schoolName = itemView.findViewById(R.id.schoolName);
            schoolDescription = itemView.findViewById(R.id.schoolDescription);
            schoolImage = itemView.findViewById(R.id.schoolListImage);
        }

    }


    @NonNull
    @Override
    public SchoolListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = inflater.inflate(R.layout.school_list_item_view, parent, false);
        return new ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //Setup up on click listeners
        holder.schoolName.setText(presenter.getSchools().get(position).getSchoolName());
        holder.schoolDescription.setText(presenter.getSchools().get(position).getSchoolDescription());
        holder.schoolImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.school));

        holder.itemView.setOnClickListener(v -> {
            //open the new intent.
            Intent intent = new Intent(context, SchoolDetails.class);
            intent.putExtra("object", presenter.getSchools().get(position));
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return presenter.getSchools().size();
    }

}
