package com.example.a20211013_igornefedov_nycschools.Screens.MainScreen;

import com.example.a20211013_igornefedov_nycschools.BasePresenter;
import com.example.a20211013_igornefedov_nycschools.BaseView;
import com.example.a20211013_igornefedov_nycschools.Data.SchoolModel;
import java.util.ArrayList;

public class MainContract {
    interface View extends BaseView{
        void showLoading(boolean show); //if true, show loading spinner, else, hide loader
        void notifyDataChanged();//update list to match the data
    }

    interface Presenter extends BasePresenter{
        void loadAllSchools();
        ArrayList<SchoolModel> getSchools(); //if String == "", return all schools
    }
}
