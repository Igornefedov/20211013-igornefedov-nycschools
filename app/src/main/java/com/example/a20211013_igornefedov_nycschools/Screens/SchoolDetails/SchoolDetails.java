package com.example.a20211013_igornefedov_nycschools.Screens.SchoolDetails;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.a20211013_igornefedov_nycschools.Data.SchoolModel;
import com.example.a20211013_igornefedov_nycschools.R;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class SchoolDetails extends AppCompatActivity implements SchoolDetailsContract.View {

    SchoolDetailsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_details);
        setPresenter(this);
    }

    @Override
    public void setPresenter(Object Presenter) {
        this.presenter = new SchoolDetailsPresenter(this);

        this.presenter.setSelectedSchool((SchoolModel)getIntent().getSerializableExtra("object"));
        presenter.start();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void initViews() {
        //init the views
        ((TextView)findViewById(R.id.schoolNameTextView)).setText(presenter.getSelectedSchool().getSchoolName());
        ((TextView)findViewById(R.id.schoolIdTextView)).setText(presenter.getSelectedSchool().getDbn());
        ((TextView)findViewById(R.id.schoolCityTextView)).setText(presenter.getSelectedSchool().getCity());
        ((TextView)findViewById(R.id.schoolDescriptionTextView)).setText(presenter.getSelectedSchool().getSchoolDescription());

        //TODO:
        //1. Move the string to either resources or the XML File
        //SAT scores
        ((TextView)findViewById(R.id.schoolNumberOfTakersTextView)).setText("Takers: " + presenter.getSelectedSchool().getNumberOfTestsTaken());
        ((TextView)findViewById(R.id.schoolWritingScoreTextView)).setText("Writing avr: " +presenter.getSelectedSchool().getSatWritingAvg());
        ((TextView)findViewById(R.id.schoolReadingScoreTextView)).setText("Reading avr: " +presenter.getSelectedSchool().getSatReadingAvg());
        ((TextView)findViewById(R.id.schoolMathScoreTextView)).setText("Math avr: " +presenter.getSelectedSchool().getSatMathAvg());

        findViewById(R.id.schoolBackButton).setOnClickListener(v -> {
            finish();
        });

        findViewById(R.id.callSchoolImage).setOnClickListener(v -> {
            try {
                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                dialIntent.setData(Uri.parse("tel:" + presenter.getSelectedSchool().getPhoneNumber()));
                startActivity(dialIntent);
            }catch (Exception e){
                e.printStackTrace();
            }
        });

        findViewById(R.id.emailSchoolImage).setOnClickListener(v -> {
            try {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + presenter.getSelectedSchool().getEmail()));
                startActivity(Intent.createChooser(emailIntent, "Select application:"));
            }catch (Exception e){
                e.printStackTrace();
            }
        });

        findViewById(R.id.schoolNavImage).setOnClickListener(v -> {
            try {
                String escapedQuery = URLEncoder.encode(presenter.getSelectedSchool().getSchoolAddress(), "UTF-8");
                Uri uri = Uri.parse("http://www.google.com/search?q=" + escapedQuery);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        findViewById(R.id.schoolWebsiteImage).setOnClickListener(v -> {
            try{
                Uri uri = Uri.parse("https://" + presenter.getSelectedSchool().getWebsite());
                Intent intent = new Intent(Intent.ACTION_VIEW,  uri);
                startActivity(intent);
            }catch (Exception e){
                e.printStackTrace();
            }
        });
    }
}