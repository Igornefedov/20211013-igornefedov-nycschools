package com.example.a20211013_igornefedov_nycschools;

public interface BasePresenter {
    void start();
}
