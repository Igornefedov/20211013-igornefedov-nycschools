package com.example.a20211013_igornefedov_nycschools.Data;

import java.io.Serializable;

public class SchoolModel implements Serializable {

    //main attributes
    private String dbn; //School id
    private String schoolName;
    private String schoolDescription;
    private String city;
    private String phoneNumber;
    private String email;
    private String website;
    private String schoolAddress;

    //additional attributes.
    private String numberOfTestsTaken = "Not unavailable";
    private String satReadingAvg = "Not unavailable";
    private String satMathAvg = "Not unavailable";
    private String satWritingAvg = "Not unavailable";

    public SchoolModel(String dbn, String schoolName, String schoolDescription) {
        this.dbn = dbn;
        this.schoolName = schoolName;
        this.schoolDescription = schoolDescription;
    }

    public SchoolModel(){

    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSchoolDescription() {
        return schoolDescription;
    }

    public void setSchoolDescription(String schoolDescription) {
        this.schoolDescription = schoolDescription;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(String schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public String getNumberOfTestsTaken() {
        return numberOfTestsTaken;
    }

    public void setNumberOfTestsTaken(String numberOfTestsTaken) {
        this.numberOfTestsTaken = numberOfTestsTaken;
    }

    public String getSatReadingAvg() {
        return satReadingAvg;
    }

    public void setSatReadingAvg(String satReadingAvg) {
        this.satReadingAvg = satReadingAvg;
    }

    public String getSatMathAvg() {
        return satMathAvg;
    }

    public void setSatMathAvg(String satMathAvg) {
        this.satMathAvg = satMathAvg;
    }

    public String getSatWritingAvg() {
        return satWritingAvg;
    }

    public void setSatWritingAvg(String satWritingAvg) {
        this.satWritingAvg = satWritingAvg;
    }
}
