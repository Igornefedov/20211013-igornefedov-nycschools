package com.example.a20211013_igornefedov_nycschools.Data;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataSource {

    static final String school_set = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json";
    static final String sat_set = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json";

    private ArrayList<SchoolModel> allSchools;
    private Context context;

    public DataSource(Context context){
        this.context = context;
    }

    public void getData(DataCallCompleteCallback callback){
        allSchools = new ArrayList<>();

        //Hashmap to map ids to indexes in the school array
        HashMap<String, Integer> existingIds = new HashMap<>();

        makeAPIRequest(school_set, jsonArray->{
            for (int i=0; i<jsonArray.length(); i++){
                try{
                    JSONObject item = jsonArray.getJSONObject(i);
                    SchoolModel model = new SchoolModel();
                    model.setDbn(!item.isNull("dbn") ? item.getString("dbn") : "Dbn unavailable");
                    model.setSchoolName(!item.isNull("school_name") ? item.getString("school_name") : "Name unavailable");
                    model.setSchoolDescription(!item.isNull("overview_paragraph") ? item.getString("overview_paragraph") : "Description unavailable");
                    model.setCity(!item.isNull("city") ? item.getString("city") : "City unavailable");
                    model.setEmail(!item.isNull("school_email") ? item.getString("school_email") : "Email unavailable");
                    model.setPhoneNumber(!item.isNull("phone_number") ? item.getString("phone_number") : "Phone unavailable");
                    model.setWebsite(!item.isNull("website") ? item.getString("website") : "Website unavailable");
                    model.setSchoolAddress(!item.isNull("primary_address_line_1") ? item.getString("primary_address_line_1") : "Address unavailable");
                    allSchools.add(model);
                    existingIds.put(model.getDbn(), allSchools.size()-1);//save the index of this data point
                }catch (JSONException e){
                    System.out.println(e.getMessage());
                }

            }
            addSatScores(existingIds, callback);
        });


    }

    private void addSatScores(HashMap<String, Integer> existingIds, DataCallCompleteCallback callback){
        makeAPIRequest(sat_set, jsonArray->{
            //Get SAT data for the class items.
            for (int i=0; i<jsonArray.length(); i++){
                try{
                    JSONObject item = jsonArray.getJSONObject(i);
                    String id = !item.isNull("dbn") ? item.getString("dbn") : null;

                    if (id != null && existingIds.containsKey(id)){
                        int indx = existingIds.get(id);
                        allSchools.get(indx).setNumberOfTestsTaken(!item.isNull("num_of_sat_test_takers") ? item.getString("num_of_sat_test_takers") : "Not unavailable");
                        allSchools.get(indx).setSatReadingAvg(!item.isNull("sat_critical_reading_avg_score") ? item.getString("sat_critical_reading_avg_score") : "Not unavailable");
                        allSchools.get(indx).setSatWritingAvg(!item.isNull("sat_writing_avg_score") ? item.getString("sat_writing_avg_score") : "Not unavailable");
                        allSchools.get(indx).setSatMathAvg(!item.isNull("sat_math_avg_score") ? item.getString("sat_math_avg_score") : "Not unavailable");
                    }
                }catch (JSONException e){
                    System.out.println("errors: " + e.getMessage());
                }
            }
            callback.onFinished(true);
        });
    }

    public ArrayList<SchoolModel> getAllSchools(){
        return allSchools;
    }

    public interface DataLoadedCallback{
        void onFinished(JSONArray response);
    }

    public interface DataCallCompleteCallback{
        void onFinished(boolean success);
    }

    private void makeAPIRequest(String url, DataLoadedCallback callback){
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        try{
                            callback.onFinished(response);
                        }catch (Exception e){
                            System.out.println(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        System.out.println("failed!! " + error.getMessage());
                    }
                });

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(jsonObjectRequest);

    }

}

